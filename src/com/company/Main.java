package com.company;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static List<ThreadSimulator> arrayList;
    public static int quota = 3;
    public static boolean isBoolean = true;
    public static int threadSum = 0;
    public static int executeThread = 0;

    public static void main(String[] args) {
        arrayList = new ArrayList<>();
        arrayList.add(new ThreadSimulator("th1", 0, 25, 10));
        arrayList.add(new ThreadSimulator("th2", 0, 30, 5));
        arrayList.add(new ThreadSimulator("th3", 0, 60, 0));
        for (int i = 0; isBoolean; i++) {
            if (!(arrayList.get(i).timeOnTotalTime >= 0)) {
                arrayList.get(i).timeOnTotalTime = arrayList.get(i).timeOnTotalTime - quota;
                continue;
            }
            if (!(arrayList.get(i).executeTime >= arrayList.get(i).totalTime)) {
                arrayList.get(i).totalTime = arrayList.get(i).totalTime - quota;
                arrayList.get(i).quotaQuality++;
            } else {
                for (ThreadSimulator th : arrayList) {
                    if (th.totalTime <= 0) {
                        executeThread++;
                    }
                }
            }

            if (executeThread == arrayList.size()) {
                isBoolean = false;
            } else {
                executeThread = 0;
            }
            if (i >= (arrayList.size() - 1)) {
                i = -1;
            }

        }
        for (ThreadSimulator thread : arrayList) {
            threadSum = threadSum + thread.quotaQuality;
            System.out.println("Thread - " + thread.name + " = " + thread.quotaQuality);
        }
        System.out.println("Their average value = " + threadSum / arrayList.size());
    }


}

